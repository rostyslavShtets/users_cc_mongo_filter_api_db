package main

import (
	"users_cc_mongo_filter_api_db/db"
	"users_cc_mongo_filter_api_db/api"
	"users_cc_mongo_filter_api_db/utils"
	"github.com/go-chi/chi"
	"net/http"
	"fmt"
	"log"
)
const port = ":8081"

func main() {
	utils.Hello()
	fmt.Println("new_new")
	db.InitDB("mongodb:27017")
			
	r := chi.NewRouter()
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(".root"))
	})

	//request for people
	r.Get("/people", api.GetAllPeople)
	r.Get("/people/{personID}", api.GetPerson)
	r.Get("/api/people", api.GetPeopleFilter)
	r.Get("/api_object/people", api.GetPeopleFilterObject)
	r.Post("/people", api.CreatePerson)
	r.Put("/people/{personID}", api.UpdatePerson)
	r.Delete("/people/{personID}", api.DeletePerson)
	
	//request for books
	r.Get("/books", api.GetAllBooks)
	r.Get("/books/{bookID}", api.GetBook)
	r.Post("/books", api.CreateBook)
	r.Delete("/books/{bookID}", api.DeleteBook)
	r.Put("/books/{bookID}", api.UpdateBook)


	fmt.Printf("Serv on port %v....\n", port)
	if err := http.ListenAndServe(port, r); err != nil {
		log.Fatal(err)
	}
	// log.Fatal(http.ListenAndServe(port, r))
}

