package api

import (
	"users_cc_mongo_filter_api_db/db"
	"users_cc_mongo_filter_api_db/models"
	"net/http"
	"log"
	"encoding/json"
	"github.com/go-chi/chi"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
)

func GetAllPeople(w http.ResponseWriter, r *http.Request) {
	res, err := db.GetAllPeopleFromDB()
	if err != nil {
		log.Fatal(err)
		return
	}
	json.NewEncoder(w).Encode(res)
}

func GetPerson(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "personID")
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	
	rs, err := db.GetOnePersonFromDB(id)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	json.NewEncoder(w).Encode(rs)
	
}

func CreatePerson(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	var person models.Person
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &person)
	if err != nil {
			log.Println("handlers SaveID error:", err)
			http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
			return
		}
		fmt.Println(person)
			
	person.Id = bson.NewObjectId()

	if err = db.CreatePersonInDB(person); err != nil {
		fmt.Println("error with insert")
		http.Error(w, http.StatusText(500), 500)
		return
	}

	fmt.Fprintf(w, "Person with ID %v created successfully\n", person.Id)
}

func DeletePerson(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "personID")
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	fmt.Println("id:", id)
	
	if err := db.DeletePersonInDB(id); err != nil {
		http.Error(w, http.StatusText(500), 500)
		fmt.Println("can't delete:", err)
		return
	}
	fmt.Fprintf(w, "Person with ID = %v deleted successfully\n", id)
}

func UpdatePerson(w http.ResponseWriter, r *http.Request) {
		//get ID from request
		id := chi.URLParam(r, "personID")
		if id == "" {
			http.Error(w, http.StatusText(400), 400)
		}
		fmt.Println("id - ", id)
	
		//read JSON from request body
		body, err := ioutil.ReadAll(r.Body)

		var person models.Person
		if err != nil {
			log.Println("handlers SaveIDID error:", err)
			http.Error(w, "can’t read body", http.StatusBadRequest)
			return
		}

		err = json.Unmarshal(body, &person)
		if err != nil {
			log.Println("handlers SaveID error:", err)
			http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
			return
		}
	
		if err = db.UpdatePersonInDb(id, &person); err != nil {
			fmt.Println("error with update")
			http.Error(w, http.StatusText(500), 500)
			return
		}
		fmt.Fprintf(w, "Person with ID = %v updated successfully\n", id)
}

// work with books
func GetAllBooks(w http.ResponseWriter, r *http.Request) {
	log.Println("qsqqss")
	res, err := db.GetAllBooksFromDB()
	if err != nil {
		log.Fatal(err)
		return
	}
	json.NewEncoder(w).Encode(res)
}

func GetBook(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "bookID")
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	
	rs, err := db.GetOneBookFromDB(id)
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	json.NewEncoder(w).Encode(rs)
	
}

func CreateBook(w http.ResponseWriter, r *http.Request) {
	fmt.Println("qsqqss")
	body, err := ioutil.ReadAll(r.Body)
	var book models.Book
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &book)
	if err != nil {
			log.Println("handlers SaveID error:", err)
			http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
			return
		}
		fmt.Println(book)
			
	book.Id = bson.NewObjectId()

	if err = db.CreateBookInDB(book); err != nil {
		fmt.Println("error with insert")
		http.Error(w, http.StatusText(500), 500)
		return
	}

	fmt.Fprintf(w, "Person with ID %v created successfully\n", book.Id)
}

func DeleteBook(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "bookID")
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	fmt.Println("id:", id)
	
	if err := db.DeleteBookInDB(id); err != nil {
		http.Error(w, http.StatusText(500), 500)
		fmt.Println("can't delete:", err)
		return
	}
	fmt.Fprintf(w, "Person with ID = %v deleted successfully\n", id)
}

func UpdateBook(w http.ResponseWriter, r *http.Request) {
	//get ID from request
	id := chi.URLParam(r, "bookID")
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	fmt.Println("id - ", id)

	//read JSON from request body
	body, err := ioutil.ReadAll(r.Body)

	var book models.Book
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}

	err = json.Unmarshal(body, &book)
	if err != nil {
		log.Println("handlers SaveID error:", err)
		http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
		return
	}

	if err = db.UpdateBookInDb(id, &book); err != nil {
		fmt.Println("error with update", err)
		http.Error(w, http.StatusText(500), 500)
		return
	}
	fmt.Fprintf(w, "Person with ID = %v updated successfully\n", id)
}

