package db

import (
	"gopkg.in/mgo.v2"
	// "gopkg.in/mgo.v2/bson"
)


// Index people
func IndexPeople() {
	indexPeople := mgo.Index{
		Key:        []string{"lastname"},
		Unique:     false,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	errIndexPeople := collectionPeople().EnsureIndex(indexPeople)
	if errIndexPeople != nil {
		panic(errIndexPeople)
	}
}

func IndexBooks() {
	indexBooks := mgo.Index{
		Key:        []string{"author"},
		Unique:     false,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	errIndexBooks := collectionBooks().EnsureIndex(indexBooks)
	if errIndexBooks != nil {
		panic(errIndexBooks)
	}
}