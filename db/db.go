package db

import (
	"log"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"users_cc_mongo_filter_api_db/models"
	"fmt"
)

var db *mgo.Database

func InitDB(connectionString string) {
	// session, err := mgo.Dial("localhost/api_db")
	session, err := mgo.Dial(connectionString)

	session.SetMode(mgo.Monotonic, true)

	if err != nil {
		log.Fatalf("Failed to connect to database: %v", err)
	}

	db = session.DB("testdb")
	
	// indexing collections
	IndexPeople()
	IndexBooks()
}

func collectionPeople() *mgo.Collection {
	return db.C("people")
}

// it is temporary fucntion to export for api filter
func CollectionPeople() *mgo.Collection {
	return db.C("people")
}

func collectionBooks() *mgo.Collection {
	return db.C("books")
}

// GetAll returns all items from the database.
func GetAllPeopleFromDB() ([]models.Person, error) {
	res := []models.Person{}

	if err := collectionPeople().Find(nil).All(&res); err != nil {
		return nil, err
	}

	return res, nil
}

// GetOne returns a single people from the database.
func GetOnePersonFromDB(id string) (*models.Person, error) {
	res := models.Person{}
	
	if err :=  collectionPeople().FindId(bson.ObjectIdHex(id)).One(&res); err != nil {
		return nil, err
	}

	return &res, nil
}

func CreatePersonInDB(person models.Person) error {
	return collectionPeople().Insert(person)
}

func DeletePersonInDB(id string) error {
	fmt.Println(id)
	return collectionPeople().Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}

func UpdatePersonInDb(id string, person *models.Person) error {
	return collectionPeople().Update(bson.M{"_id": bson.ObjectIdHex(id)}, &person)
}


// work with book
func GetAllBooksFromDB() ([]models.Book, error) {
	res := []models.Book{}

	if err := collectionBooks().Find(nil).All(&res); err != nil {
		return nil, err
	}

	return res, nil
}

func GetOneBookFromDB(id string) (*models.Book, error) {
	res := models.Book{}
	
	if err :=  collectionBooks().FindId(bson.ObjectIdHex(id)).One(&res); err != nil {
		return nil, err
	}

	return &res, nil
}

func CreateBookInDB(book models.Book) error {
	return collectionBooks().Insert(book)
}

func DeleteBookInDB(id string) error {
	fmt.Println(id)
	return collectionBooks().Remove(bson.M{"_id": bson.ObjectIdHex(id)})
}

func UpdateBookInDb(id string, book *models.Book) error {
	return collectionBooks().Update(bson.M{"_id": bson.ObjectIdHex(id)}, &book)
}

